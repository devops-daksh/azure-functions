# azure-functions

It describes sample project, how create azure functions and call them

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/devops-daksh/azure-functions.git
git branch -M main
git push -uf origin main
```

##  Pre-Requisites
- [ ] Java Developer Kit, version 8
- [ ] [Azure CLI](https://docs.microsoft.com/en-us/cli/azure)
- [ ] [Azure Functions Core Tools version](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local#v2) 2.6.666 or above
- [ ] [Apache Maven](https://maven.apache.org/), version 3.0 or above.

## Steps

- In an empty folder, run the following command to generate the Functions project from a Maven archetype
``` 
mvn archetype:generate "-DarchetypeGroupId=com.microsoft.azure" "-DarchetypeArtifactId=azure-functions-archetype" "-DjavaVersion=8" 

or  

mvn archetype:generate -DarchetypeGroupId=com.microsoft.azure -DarchetypeArtifactId=azure-functions-archetype -DappName=azure-fucntions-triggers -DappRegion=Central US -DresourceGroup=azure-fucntions-triggers_group -DgroupId=com.{functionAppName}.group -DartifactId={functionAppName}-functions -Dpackage=com.{functionAppName} -DinteractiveMode=false
```
- Maven asks you for values needed to finish generating the project on deployment.
Provide the following values when prompted:

```
groupId 	com.demo 	A value that uniquely identifies your project across all projects, following the package naming rules for Java.
artifactId 	azure-functions 	A value that is the name of the jar, without a version number.
version 	1.0-SNAPSHOT 	Choose the default value.
package 	com.demo 	A value that is the Java package for the generated function code. Use the default.
```
- Type Y or press Enter to confirm.
    Maven creates the project files in a new folder with a name of artifactId, which in this example is azure-functions.

Settings for the Azure resources created to host your app are defined in the configuration element of the plugin with a groupId of com.microsoft.azure in the generated pom.xml file. For example, the configuration element below instructs a Maven-based deployment to create a function app in the java-functions-group resource group in the westus region. The function app itself runs on Windows hosted in the java-functions-app-service-plan plan, which by default is a serverless Consumption plan.

Settings can be changed in pom.xml like region, service-plan name, storage etc

## MVN commands

```
mvn clean package  - create a build
mvn azure-functions:run - to run the httpFucntion azure function locally
mvn azure-functions:deploy - to deploy azure functions on azure cloud
```

## Following resources will be created on azure portal

- Resourse group
- Storage account with name if given in pom.xml. It have containers
    - azure-webjobs-hosts
    - azure-webjobs-secrets
    - java-functions-run-from-packages
    - scm-releases
- Function APP with given name
- App Service plan
- Application Insights (if not marked disabled)

## How run azure function

- Open Fucntion App and copy the url like https://azure-functions-hello.azurewebsites.net


## More details

- https://docs.microsoft.com/en-us/azure/azure-functions/create-first-function-cli-java?tabs=cmd%2Cazure-cli%2Cbrowser
- https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local?tabs=v4%2Cwindows%2Cjava%2Cportal%2Cbash#v2
